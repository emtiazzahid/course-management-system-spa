## Run Project

- `composer install` to install vendor files
- `cp .env.example .env` create environment file
-  Change environment values from .env file
- `php artisan config:cache`  to cache updated variables
- `php artisan migrate --seed` create db tables and insert dummy data
- `php artisan serve` to serve the project (default port: 8000)