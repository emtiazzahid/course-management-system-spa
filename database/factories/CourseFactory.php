<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;


$factory->define(\App\Models\Course::class, function (\Faker\Generator $faker) {
    return [
        'title' => $faker->sentence(5),
        'sub_title' => $faker->sentence(20),
        'description' => $faker->text(),
        'user_id' => 1,
    ];
});