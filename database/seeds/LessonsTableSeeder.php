<?php

use Illuminate\Database\Seeder;

class LessonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Course::class, 50)->create()->each(function ($course) {
            $course->lessons()->saveMany(factory(\App\Models\Lesson::class, rand(2, 10))->make());
        });
    }
}
