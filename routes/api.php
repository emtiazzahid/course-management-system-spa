<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('auth')->group(function () {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::get('refresh', 'AuthController@refresh');
    Route::group(['middleware' => 'jwt.auth'], function(){
        Route::get('user', 'AuthController@user');
        Route::post('logout', 'AuthController@logout');
    });
});

Route::group(['middleware' => 'jwt.auth'], function(){
    Route::apiResource('courses', 'API\CourseController');
    Route::apiResource('lessons', 'API\LessonController');
});

Route::prefix('frontend')->group(function () {
    Route::get('courses', 'API\Frontend\CourseController@index');
    Route::get('course/{id}', 'API\Frontend\CourseController@show');
});


