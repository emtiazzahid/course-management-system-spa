<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $appends = ['placeholder_image'];
    protected $guarded = [];

    public function lessons()
    {
        return $this->hasMany(Lesson::class);
    }

    /*
     * Placeholder image for course
     */

    public function getPlaceholderImageAttribute()
    {
        return url('/images/course.png');
    }

}
