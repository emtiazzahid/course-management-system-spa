<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\LessonRequest;
use App\Models\Course;
use App\Models\Lesson;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LessonController extends Controller
{
    public function index()
    {
        $lessons = Lesson::with('course')
            ->whereHas('course' , function ($q) {
            $q->where('user_id', Auth::user()->id);
        })->latest()
            ->get();

        return response()->json([
            "lessons" => $lessons
        ], 200);
    }
    public function show($id)
    {
        $lesson = Lesson::with('course')->whereId($id)->first();
        return response()->json([
            "lesson" => $lesson
        ], 200);
    }
    public function store(LessonRequest $request)
    {
        $lesson = Lesson::create([
            'title' => $request->title,
            'sub_title' => $request->sub_title,
            'course_id' => $request->course,
        ]);
        return response()->json([
            "lesson" => $lesson
        ], 200);
    }
}
