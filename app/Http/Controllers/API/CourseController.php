<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\CourseRequest;
use App\Models\Course;
use Illuminate\Support\Facades\Auth;

class CourseController extends Controller
{
    public function index()
    {
        $courses = Course::where('user_id', Auth::user()->id)
            ->latest()
            ->get();

        return response()->json([
            "courses" => $courses
        ], 200);
    }
    public function show($id)
    {
        $course = Course::whereId($id)
            ->where('user_id', Auth::user()->id)
            ->first();

        return response()->json([
            "course" => $course
        ], 200);
    }
    public function store(CourseRequest $request)
    {
        $course = Course::create([
            'title' => $request->title,
            'sub_title' => $request->sub_title,
            'description' => $request->description,
            'user_id' => Auth::user()->id,
        ]);

        return response()->json([
            "course" => $course
        ], 200);
    }
}