<?php
namespace App\Http\Controllers\API\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function index(Request $request)
    {
        $courses = Course::with('lessons')
            ->latest()
            ->paginate($request->perPage);

        return response()->json([
            "courses" => $courses
        ], 200);
    }
    public function show($id)
    {
        $course = Course::with('lessons')->whereId($id)->first();
        return response()->json([
            "course" => $course
        ], 200);
    }
}