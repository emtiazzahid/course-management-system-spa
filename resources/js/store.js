import { getLocalUser } from "./helpers/auth";

const user = getLocalUser();

export default {
    state: {
        currentUser: user,
        registeredUser: null,
        isLoggedIn: !!user,
        loading: false,
        auth_error: null,
        reg_error: null,
        courses: [],
        lessons: [],
        userCourses: [],
        userLessons: [],
        pagination: {
            page: 1,
            rowsPerPage: 12,
        }
    },
    getters: {
        isLoading(state) {
            return state.loading;
        },
        isLoggedIn(state) {
            return state.isLoggedIn;
        },
        currentUser(state) {
            return state.currentUser;
        },
        authError(state) {
            return state.auth_error;
        },
        regError(state){
            return state.reg_error;
        },
        registeredUser(state){
            return state.registeredUser;
        },
        courses(state) {
            return state.courses;
        },
        userCourses(state) {
            return state.userCourses;
        },
        lessons(state) {
            return state.lessons;
        },
        userLessons(state) {
            return state.userLessons;
        },
    },
    mutations: {
        login(state) {
            state.loading = true;
            state.auth_error = null;
        },
        loginSuccess(state, payload) {
            state.auth_error = null;
            state.isLoggedIn = true;
            state.loading = false;
            state.currentUser = Object.assign({}, payload.user, {token: payload.access_token});

            localStorage.setItem("user", JSON.stringify(state.currentUser));
        },
        loginFailed(state, payload) {
            state.loading = false;
            state.auth_error = payload.error;
        },
        registerSuccess(state, payload){
            state.reg_error = null;
            state.registeredUser = payload.user;
        },
        registerFailed(state, payload){
            state.reg_error = payload.error;
        },
        logout(state) {
            localStorage.removeItem("user");
            state.isLoggedIn = false;
            state.currentUser = null;
        },
        updateCourses(state, payload) {
            state.courses = payload;
        },
        updateUserCourses(state, payload) {
            state.userCourses = payload
        },
        updateLessons(state, payload) {
            state.lessons = payload;
        },
        updateUserLessons(state, payload) {
            state.userLessons = payload;
        },

    },
    actions: {
        login(context) {
            context.commit("login");
            context.commit('updateCourses');
            context.commit('updateLessons');
        },
        getCourses(context) {
            axios.get('/courses')
                .then((response) => {
                    context.commit('updateCourses', response.data.courses);
                })
        },
        getLessons(context) {
            axios.get('/lessons')
                .then((response) => {
                    context.commit('updateLessons', response.data.lessons);
                })
        },
        getUserCourses(context, page) {
            let page_number = page.page || this.state.pagination.page;
            let rowsPerPage = page.rowsPerPage || this.state.pagination.rowsPerPage;

            axios.get('/frontend/courses?page=' + page_number + '&perPage=' + rowsPerPage)
                .then((response) => {
                    context.commit('updateUserCourses', response.data.courses);
                })
        },
        getUserLessons(context) {
            axios.get('/frontend/lessons')
                .then((response) => {
                    context.commit('updateUserLessons', response.data.lessons);
                })
        },
    }
};