import 'es6-promise/auto'
import axios from 'axios'
import './bootstrap'
import Vue from 'vue'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'
import Index from './Index'
import router from './router'
import Vuex from 'vuex';
import {initialize} from './helpers/general';
import StoreData from './store';

import 'vuetify/dist/vuetify.min.css'

// Set Vue globally
window.Vue = Vue;
// Set Vue router
Vue.router = router;
Vue.use(VueRouter);
Vue.use(Vuex);

const store = new Vuex.Store(StoreData);


// Set Vue authentication
Vue.use(VueAxios, axios);
axios.defaults.baseURL = `${process.env.MIX_APP_URL}/api`;
initialize(store, router);

const app = new Vue({
    el: '#app',
    router,
    store,
    components: {
        Index
    }
});