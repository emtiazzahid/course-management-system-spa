import VueRouter from 'vue-router'
// Pages
import Home from './pages/Home'
import Register from './pages/Register'
import Login from './pages/Login'
import CourseMain from './pages/course/Main.vue';
import CourseList from './pages/course/List.vue';
import NewCourse from './pages/course/New.vue';
import CourseView from './pages/course/View.vue';
import LessonMain from './pages/lesson/Main.vue';
import LessonList from './pages/lesson/List.vue';
import NewLesson from './pages/lesson/New.vue';
import LessonView from './pages/lesson/View.vue';
import UserCourseView from './pages/frontend/CourseView.vue';
import UserLessonView from './pages/frontend/LessonView.vue';
// Routes
const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
        meta: {
            auth: undefined
        }
    },
    {
        path: '/course/:id',
        name: 'course_view',
        component: UserCourseView,
        meta: {
            auth: undefined
        }
    },
    {
        path: '/lesson/:id',
        name: 'lesson_view',
        component: UserLessonView,
        meta: {
            auth: undefined
        }
    },
    {
        path: '/register',
        name: 'register',
        component: Register,
        meta: {
            auth: false
        }
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            auth: false
        }
    },
    {
        path: '/courses',
        component: CourseMain,
        meta: {
            requiresAuth: true
        },
        children: [
            {
                path: '/',
                component: CourseList
            },
            {
                path: 'new',
                component: NewCourse
            },
            {
                path: ':id',
                component: CourseView
            }
        ]
    },
    {
        path: '/lessons',
        component: LessonMain,
        meta: {
            requiresAuth: true
        },
        children: [
            {
                path: '/',
                component: LessonList
            },
            {
                path: 'new',
                component: NewLesson
            },
            {
                path: ':id',
                component: LessonView
            }
        ]
    },
]
const router = new VueRouter({
    history: true,
    mode: 'history',
    routes,
})
export default router